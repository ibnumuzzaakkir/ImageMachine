package com.scoproject.imagemachine;

import android.content.Intent;
import android.support.test.espresso.intent.rule.IntentsTestRule;
import android.support.test.filters.LargeTest;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.scoproject.imagemachine.activity.home.HomeActivity;
import com.scoproject.imagemachine.activity.home.HomeActivity_;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;

/**
 * Created by Ibnu Muzzakkir on 1/31/2017.
 * Contact ibnumuzzaakkir@gmail.com
 * scoproject
 */
@RunWith(AndroidJUnit4.class)
@LargeTest
public class HomeActivityTest {
    @Rule
    public IntentsTestRule<HomeActivity_> mAddNoteIntentsTestRule =
            new IntentsTestRule<>(HomeActivity_.class);

    @Test
    public void onClickAddMachineData() throws Throwable{
        onView(withId(R.id.home_btn_machine_data)).perform(click());
        onView(withId(R.id.machinedata_add)).perform(click());
    }
}
