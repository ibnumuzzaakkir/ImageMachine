package com.scoproject.imagemachine.activity.addmachinedata;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;

import com.scoproject.imagemachine.di.AppComponent;
import com.scoproject.imagemachine.ui.BaseActivity;
import com.scoproject.imagemachine.ui.ViewPresenter;
import com.scoproject.imagemachine.ui.navigation.ActivityScreen;

/**
 * Created by ibnumuzzakkir on 01/02/2017.
 * Android Developer
 * Garena Indonesia
 */

public class AddMachineDataActivity extends BaseActivity {

    private AddMachineDataView mView;
    private AddMachineDataPresenter mPresenter;
    private AddMachineDataComponent mComponent;

    @Override
    protected void onCreateUI(Bundle bundle) {
        mView = AddMachineDataView_.build(this);
        setContentView(mView);
        mPresenter = new AddMachineDataPresenter(this);
        mComponent.inject(mPresenter);
        mPresenter.takeView(mView);
    }

    @Override
    protected void onCreateComponent(AppComponent appComponent) {
        mComponent = DaggerAddMachineDataComponent.builder().appComponent(appComponent).build();
        mComponent.inject(this);
    }

    @Override
    protected boolean isValid() {
        return false;
    }

    @Override
    protected ViewPresenter<? extends View> presenter() {
        return mPresenter;
    }

    public static class Screen extends ActivityScreen {

        public Screen() {

        }

        @Override
        protected void configureIntent(@NonNull Intent intent) {

        }

        @Override
        protected Class<? extends Activity> activityClass() {
            return AddMachineDataActivity.class;
        }
    }
}
