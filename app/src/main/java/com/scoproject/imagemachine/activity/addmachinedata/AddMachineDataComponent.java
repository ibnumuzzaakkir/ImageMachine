package com.scoproject.imagemachine.activity.addmachinedata;

import com.scoproject.imagemachine.di.AppComponent;

import dagger.Component;

/**
 * Created by ibnumuzzakkir on 01/02/2017.
 * Android Developer
 * Garena Indonesia
 */

@AddMachineDataScope
@Component(dependencies = {AppComponent.class})
public interface AddMachineDataComponent {
    void inject(AddMachineDataActivity mActivity);
    void inject(AddMachineDataPresenter mPresenter);
}