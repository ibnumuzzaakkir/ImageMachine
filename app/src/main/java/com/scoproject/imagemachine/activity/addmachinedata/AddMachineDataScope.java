package com.scoproject.imagemachine.activity.addmachinedata;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;

/**
 * Created by ibnumuzzakkir on 01/02/2017.
 * Android Developer
 * Garena Indonesia
 */

@Scope
@Retention(RetentionPolicy.RUNTIME)
public @interface AddMachineDataScope {
}

