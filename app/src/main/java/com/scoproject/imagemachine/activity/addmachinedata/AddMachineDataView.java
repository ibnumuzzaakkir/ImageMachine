package com.scoproject.imagemachine.activity.addmachinedata;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.Image;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.scoproject.imagemachine.R;
import com.scoproject.imagemachine.app.AppConst;
import com.scoproject.imagemachine.data.MachineData;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import es.guiguegon.gallerymodule.model.GalleryMedia;

/**
 * Created by ibnumuzzakkir on 01/02/2017.
 * Android Developer
 * Garena Indonesia
 */
@EViewGroup(R.layout.activity_addmachinedata)
public class AddMachineDataView extends CoordinatorLayout {

    @ViewById(R.id.addmachinedata_machine_name)
    EditText mMachineName;
    @ViewById(R.id.addmachinedata_machine_type)
    EditText mMachineType;
    @ViewById(R.id.addmachinedata_machine_last_maintenance_date)
    EditText mLastMaintenanceDate;
    @ViewById(R.id.addmachinedata_machine_barcode_number)
    EditText mBarcodeNumber;
    @ViewById(R.id.addmachinedata_machine_btn_choose_image)
    Button mMachineImage;
    @ViewById(R.id.addmachinedata_btn_submit)
    Button mSubmit;
    @ViewById(R.id.addmachinedata_machine_image_gallery)
    RecyclerView mMachineImageGallery;
    private LayoutInflater mLayoutInflater;
    private MachineImageAdapter mAdapter;
    private List<String> mMachineImageUriList = new ArrayList<>();
    SparseBooleanArray selectedItems = new SparseBooleanArray();

    public AddMachineDataView(Context context) {
        super(context);
        mLayoutInflater = LayoutInflater.from(getContext());
    }

    @AfterViews
    void init(){
        mMachineImageGallery.setLayoutManager(new GridLayoutManager(getContext(), AppConst.LIMIT_IMAGE_GALLERY.limit));
        mMachineImageGallery.setHasFixedSize(true);
        mAdapter = new MachineImageAdapter();
        mMachineImageGallery.setAdapter(mAdapter);
    }

    void loadImageList(List<String> mImageUriList){
        if(getSizeMachineImage() == 0){
            if(mImageUriList.size() != 0){
                mMachineImageUriList.addAll(mImageUriList);
            }
        }else if(getSizeMachineImage() != 0 && getSizeMachineImage() <=10){
            if(mImageUriList.size() != 0){
                mMachineImageUriList.addAll(mImageUriList);
            }
        }else{
            Log.d(getClass().getName(), "Over Limit Add New Photo Album");
        }
        mAdapter.notifyDataSetChanged();
    }
    List<String> getmMachineImageUriList(){
        return mMachineImageUriList;
    }

    int getSizeMachineImage(){
        return  mMachineImageUriList.size();
    }

    private void executeOnClick(int pos) {
        boolean isCheck = !selectedItems.get(pos);
        if (isCheck) {
            selectedItems.put(pos, true);
        } else {
            selectedItems.delete(pos);
        }
        Log.d(getClass().getName() +"ukuran", String.valueOf(selectedItems.size()));
    }

    private class MachineImageAdapter extends RecyclerView.Adapter<MachineImageViewHolder>{

        @Override
        public MachineImageViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = mLayoutInflater.inflate(R.layout.activity_addmachinedata_image_gallery_item, parent, false);
            return new MachineImageViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(MachineImageViewHolder holder, int position) {
            String imageUri = mMachineImageUriList.get(position);
            Glide.with(getContext())
                    .load(new File(imageUri))
                    .centerCrop()
                    .into(holder.mMachineImage);
/*            if(selectedItems.get(position)){
                selectedItems.put(position, false);
            }else{
                selectedItems.put(position, true);
            }*/
            holder.itemView.setOnClickListener(view -> {
                executeOnClick((int)view.getTag());
            });
            holder.mMachineImage.setOnClickListener(view -> {
                selectedItems.put(position, true);
                AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getContext());
                LayoutInflater inflater = mLayoutInflater;
                View dialogView = inflater.inflate(R.layout.activity_addmachinedata_image_fullscreen, null);
                Bitmap bitmap = BitmapFactory.decodeFile(imageUri);
                ImageView mImageFullScreen = (ImageView) dialogView.findViewById(R.id.activity_image_full_screen);
                mImageFullScreen.setImageBitmap(bitmap);
                dialogBuilder.setView(dialogView);
                AlertDialog alertDialog = dialogBuilder.create();
                alertDialog.show();
                Button mBtnDelete = (Button) dialogView.findViewById(R.id.activity_btn_delete_image);
                mBtnDelete.setOnClickListener(viewDelete -> {
                    mMachineImageUriList.remove(imageUri);
                    notifyItemRemoved(position);
                    alertDialog.dismiss();
                });
            });
        }

        @Override
        public int getItemCount() {
            return mMachineImageUriList.size();
        }
    }


    private static class MachineImageViewHolder extends RecyclerView.ViewHolder{
        ImageView mMachineImage;
        public MachineImageViewHolder(View itemView) {
            super(itemView);
            mMachineImage = (ImageView) itemView.findViewById(R.id.addmachinedata_machine_image_gallery);
        }
    }
}
