package com.scoproject.imagemachine.activity.codereader;

import android.os.Bundle;
import android.util.Log;

import com.google.zxing.Result;
import com.scoproject.imagemachine.activity.machinedetail.EditMachineDetailActivity;
import com.scoproject.imagemachine.activity.machinedetail.MachineDetailActivity;
import com.scoproject.imagemachine.app.AppConst;
import com.scoproject.imagemachine.di.AppComponent;
import com.scoproject.imagemachine.ui.BaseActivity;
import com.scoproject.imagemachine.ui.navigation.ActivityScreenSwitcher;

import javax.inject.Inject;

import me.dm7.barcodescanner.zxing.ZXingScannerView;

/**
 * Created by Ibnu Muzzakkir on 2/2/2017.
 * Contact ibnumuzzaakkir@gmail.com
 * scoproject
 */

public class CodeReaderActivity extends BaseActivity implements ZXingScannerView.ResultHandler {
    @Inject
    ActivityScreenSwitcher mScreenSwitcher;
    private ZXingScannerView mScannerView;
    private CodeReaderComponent mComponent;

    @Override
    protected void onCreateUI(Bundle bundle) {
        mScannerView = new ZXingScannerView(this);
        setContentView(mScannerView);
    }


    @Override
    protected void onCreateComponent(AppComponent appComponent) {
        mComponent = DaggerCodeReaderComponent.builder().appComponent(appComponent).build();
        mComponent.inject(this);
    }


    @Override
    public void onResume() {
        super.onResume();
        mScannerView.setResultHandler(this);
        mScannerView.startCamera();
    }

    @Override
    public void onPause() {
        super.onPause();
        mScannerView.stopCamera();
    }

    @Override
    protected boolean isValid() {
        return false;
    }

    @Override
    public void handleResult(Result rawResult) {
        // Do something with the result here
        Log.v("TestBarcode", rawResult.getText()); // Prints scan results
        Log.v("TestBarcode", rawResult.getBarcodeFormat().toString()); // Prints the scan format (qrcode, pdf417 etc.)
        mScreenSwitcher.open(new MachineDetailActivity.Screen(Long.parseLong(rawResult.getText()), AppConst.SOURCE_ACTIVITY.MACHINE_BARCODE_ACTIVITY));
        // If you would like to resume scanning, call this method below:
        mScannerView.resumeCameraPreview(this);
    }
}
