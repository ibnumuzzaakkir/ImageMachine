package com.scoproject.imagemachine.activity.codereader;

import com.scoproject.imagemachine.di.AppComponent;

import dagger.Component;

/**
 * Created by Ibnu Muzzakkir on 2/2/2017.
 * Contact ibnumuzzaakkir@gmail.com
 * scoproject
 */

@CodeReaderScope
@Component(dependencies = {AppComponent.class})
public interface CodeReaderComponent {
    void inject(CodeReaderActivity mActivity);
}
