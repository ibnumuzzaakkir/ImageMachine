package com.scoproject.imagemachine.activity.codereader;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;

/**
 * Created by Ibnu Muzzakkir on 2/2/2017.
 * Contact ibnumuzzaakkir@gmail.com
 * scoproject
 */

@Scope
@Retention(RetentionPolicy.RUNTIME)
public @interface CodeReaderScope {
}

