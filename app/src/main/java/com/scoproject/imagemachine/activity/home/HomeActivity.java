package com.scoproject.imagemachine.activity.home;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;


import com.scoproject.imagemachine.R;
import com.scoproject.imagemachine.activity.codereader.CodeReaderActivity;
import com.scoproject.imagemachine.activity.machinedata.MachineDataActivity;
import com.scoproject.imagemachine.ui.navigation.ActivityScreenSwitcher;

import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;

import javax.inject.Inject;

/**
 * Created by ibnumuzzakkir on 31/01/2017.
 * Android Developer
 * Garena Indonesia
 */
@EActivity(R.layout.activity_home)
public class HomeActivity extends AppCompatActivity {

    @Click(R.id.home_btn_machine_data)
    void mMachineOnClick(){
        Intent intent = new Intent(this, MachineDataActivity.class);
        startActivity(intent);
    }

    @Click(R.id.home_btn_code_reader)
    void mCodeReaderOnClick(){
        Intent intent = new Intent(this, CodeReaderActivity.class);
        startActivity(intent);
    }
}
