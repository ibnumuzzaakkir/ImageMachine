package com.scoproject.imagemachine.activity.machinedata;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;

import com.scoproject.imagemachine.di.AppComponent;
import com.scoproject.imagemachine.ui.BaseActivity;
import com.scoproject.imagemachine.ui.ViewPresenter;
import com.scoproject.imagemachine.ui.navigation.ActivityScreen;

/**
 * Created by Ibnu Muzzakkir on 2/1/2017.
 * Contact ibnumuzzaakkir@gmail.com
 * scoproject
 */

public class MachineDataActivity extends BaseActivity {
    private MachineDataComponent mComponent;
    private MachineDataPresenter mPresenter;
    private MachineDataView mView;

    @Override
    protected void onCreateUI(Bundle bundle) {
        mView = MachineDataView_.build(this);
        setContentView(mView);
        mPresenter = new MachineDataPresenter(this);
        mComponent.inject(mPresenter);
        mPresenter.takeView(mView);
    }

    @Override
    protected void onCreateComponent(AppComponent appComponent) {
        mComponent = DaggerMachineDataComponent.builder().appComponent(appComponent).build();
        mComponent.inject(this);
    }

    @Override
    protected boolean isValid() {
        return false;
    }

    @Override
    protected ViewPresenter<? extends View> presenter() {
        return mPresenter;
    }

    public static class Screen extends ActivityScreen {
        public Screen() {

        }

        @Override
        protected void configureIntent(@NonNull Intent intent) {

        }

        @Override
        protected Class<? extends Activity> activityClass() {
            return MachineDataActivity.class;
        }
    }
}
