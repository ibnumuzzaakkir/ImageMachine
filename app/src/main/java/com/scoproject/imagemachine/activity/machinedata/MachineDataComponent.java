package com.scoproject.imagemachine.activity.machinedata;

import com.scoproject.imagemachine.di.AppComponent;

import dagger.Component;

/**
 * Created by Ibnu Muzzakkir on 2/1/2017.
 * Contact ibnumuzzaakkir@gmail.com
 * scoproject
 */
@MachineDataScope
@Component(dependencies = {AppComponent.class})
public interface MachineDataComponent {
    void inject(MachineDataActivity mActivity);
    void inject(MachineDataPresenter mPresenter);
}
