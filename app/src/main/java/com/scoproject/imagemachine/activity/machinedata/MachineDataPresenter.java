package com.scoproject.imagemachine.activity.machinedata;

import android.support.annotation.VisibleForTesting;
import android.view.View;

import com.scoproject.imagemachine.activity.addmachinedata.AddMachineDataActivity;
import com.scoproject.imagemachine.activity.machinedetail.MachineDetailActivity;
import com.scoproject.imagemachine.app.AppConst;
import com.scoproject.imagemachine.data.MachineData;
import com.scoproject.imagemachine.model.MachineDataModel;
import com.scoproject.imagemachine.ui.ViewPresenter;
import com.scoproject.imagemachine.ui.navigation.ActivityScreenSwitcher;
import com.scoproject.imagemachine.utils.TimeHelper;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

/**
 * Created by Ibnu Muzzakkir on 2/1/2017.
 * Contact ibnumuzzaakkir@gmail.com
 * scoproject
 */

public class MachineDataPresenter extends ViewPresenter<MachineDataView> {
    @Inject
    ActivityScreenSwitcher mActivityScreenSwitcher;
    @Inject
    MachineDataModel mModel;

    private MachineDataActivity mActivity;

    public MachineDataPresenter(MachineDataActivity machineDataActivity) {
        mActivity = machineDataActivity;
    }

    private List<MachineData> machineDataList = new ArrayList<>();

    @Override
    public void onLoad(){

        getView().mDataAddBtn.setOnClickListener(view -> {
                mActivityScreenSwitcher.open(new AddMachineDataActivity.Screen());
        });

        getView().setOnClickItemListener( onClick-> {
             holderOnItemClick(onClick);

        });

        getView().loadMachineData(mModel.loadAll());
    }

    private void holderOnItemClick(View view){
        MachineData machineData = (MachineData) (view.getTag());
        mActivityScreenSwitcher.open(new MachineDetailActivity.Screen(machineData.getId(), AppConst.SOURCE_ACTIVITY.MACHINE_DATA_ACTIVITY));
    }

    /*Mock Data*/
/*    private void setMachinDataDummy(){
        for(int i = 0;i<5;i++){
            MachineData machineData = new MachineData();
            machineData.setMachine_name("Machine " + i);
            machineData.setMachine_type("Alpha " + i);
            machineData.setMachine_barcode_number(i + 00001);
            machineData.setLast_maintenance_date(TimeHelper.getFormatedDate(System.currentTimeMillis()/1000));
            mModel.save(machineData);
        }
    }*/
}
