package com.scoproject.imagemachine.activity.machinedata;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;

/**
 * Created by Ibnu Muzzakkir on 2/1/2017.
 * Contact ibnumuzzaakkir@gmail.com
 * scoproject
 */
@Scope
@Retention(RetentionPolicy.RUNTIME)
public @interface MachineDataScope {
}

