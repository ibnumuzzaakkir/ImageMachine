package com.scoproject.imagemachine.activity.machinedata;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.scoproject.imagemachine.R;
import com.scoproject.imagemachine.data.MachineData;
import com.scoproject.imagemachine.ui.navigation.ActivityScreenSwitcher;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

/**
 * Created by Ibnu Muzzakkir on 2/1/2017.
 * Contact ibnumuzzaakkir@gmail.com
 * scoproject
 */
@EViewGroup(R.layout.activity_machinedata)
public class MachineDataView extends CoordinatorLayout {
    @ViewById(R.id.machinedata_recycle_data)
    RecyclerView mRecycleData;
    @ViewById(R.id.machinedata_add)

    FloatingActionButton mDataAddBtn;
    private OnClickListener mOnClicItemListener;
    private MachineAdapter mAdapter;
    private LayoutInflater mLayoutInflater;
    private List<MachineData> mMachineDataList = new ArrayList<>();

    @AfterViews
    void init(){
        mRecycleData.setLayoutManager(new LinearLayoutManager(getContext()));
        mRecycleData.setHasFixedSize(true);
        mAdapter = new MachineAdapter();
        mRecycleData.setAdapter(mAdapter);
    }

    public MachineDataView(Context context) {
        super(context);
        mLayoutInflater = LayoutInflater.from(getContext());
    }

    void loadMachineData(@NonNull List<MachineData> machineDatas){
        mMachineDataList.clear();
        if(machineDatas.size() != 0){
            mMachineDataList.addAll(machineDatas);
        }
    }

    void setOnClickItemListener(OnClickListener onClickItemListener) {
        mOnClicItemListener = onClickItemListener;
    }

    private class MachineAdapter extends RecyclerView.Adapter<MachineViewHolder>{

        @Override
        public MachineViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = mLayoutInflater.inflate(R.layout.activity_machinedata_item, parent, false);
            itemView.setOnClickListener(mOnClicItemListener);
            return new MachineViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(MachineViewHolder holder, int position) {
            MachineData machineData = mMachineDataList.get(position);
            holder.mMachineName.setText(getResources().getString(R.string.machine_name, machineData.getMachine_name()));
            holder.mMachineType.setText(getResources().getString(R.string.machine_type, machineData.getMachine_type()));
            holder.itemView.setTag(machineData);
        }

        @Override
        public int getItemCount() {
            return mMachineDataList.size();
        }
    }

    private static class MachineViewHolder extends RecyclerView.ViewHolder{
        TextView mMachineName;
        TextView mMachineType;
        public MachineViewHolder(View itemView) {
            super(itemView);
            mMachineName = (TextView) itemView.findViewById(R.id.machinedata_item_machine_name);
            mMachineType = (TextView) itemView.findViewById(R.id.machinedata_item_machine_type);
        }
    }
}
