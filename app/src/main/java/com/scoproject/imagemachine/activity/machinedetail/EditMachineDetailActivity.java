package com.scoproject.imagemachine.activity.machinedetail;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;

import com.scoproject.imagemachine.di.AppComponent;
import com.scoproject.imagemachine.ui.BaseActivity;
import com.scoproject.imagemachine.ui.ViewPresenter;
import com.scoproject.imagemachine.ui.navigation.ActivityScreen;

/**
 * Created by Ibnu Muzzakkir on 2/2/2017.
 * Contact ibnumuzzaakkir@gmail.com
 * scoproject
 */

public class EditMachineDetailActivity extends BaseActivity {
    private EditMachineDetailPresenter mPresenter;
    private EditMachineDetailView mView;
    private MachineDetailComponent mComponent;
    @Override
    protected void onCreateUI(Bundle bundle) {
        mView = EditMachineDetailView_.build(this);
        setContentView(mView);
        long mId = getIntent().getLongExtra(Screen.MACHINE_DETAIL_DATA, 0);
        mPresenter = new EditMachineDetailPresenter(this, mId);
        mComponent.inject(mPresenter);
        mPresenter.takeView(mView);
    }

    @Override
    protected void onCreateComponent(AppComponent appComponent) {
        mComponent = DaggerMachineDetailComponent.builder().appComponent(appComponent).build();
        mComponent.inject(this);
    }

    @Override
    protected boolean isValid() {
        return false;
    }

    protected ViewPresenter<? extends View> presenter() {
        return mPresenter;
    }

    public static class Screen extends ActivityScreen {
        private static final String MACHINE_DETAIL_DATA = "machine_detail_data";

        private long mId;

        public Screen(long id) {
            this.mId = id;
        }

        @Override
        protected void configureIntent(@NonNull Intent intent) {
            intent.putExtra(MACHINE_DETAIL_DATA, mId);
        }

        @Override
        protected Class<? extends Activity> activityClass() {
            return EditMachineDetailActivity.class;
        }
    }
}
