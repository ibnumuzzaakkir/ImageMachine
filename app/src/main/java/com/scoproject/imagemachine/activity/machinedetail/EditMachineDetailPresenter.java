package com.scoproject.imagemachine.activity.machinedetail;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import com.scoproject.imagemachine.activity.machinedata.MachineDataActivity;
import com.scoproject.imagemachine.app.AppConst;
import com.scoproject.imagemachine.data.MachineData;
import com.scoproject.imagemachine.model.MachineDataModel;
import com.scoproject.imagemachine.ui.ViewPresenter;
import com.scoproject.imagemachine.ui.navigation.ActivityScreenSwitcher;
import com.scoproject.imagemachine.utils.UIhelper;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import javax.inject.Inject;

import es.guiguegon.gallerymodule.GalleryActivity;
import es.guiguegon.gallerymodule.GalleryHelper;
import es.guiguegon.gallerymodule.model.GalleryMedia;

/**
 * Created by Ibnu Muzzakkir on 2/2/2017.
 * Contact ibnumuzzaakkir@gmail.com
 * scoproject
 */

public class EditMachineDetailPresenter extends ViewPresenter<EditMachineDetailView> {
    @Inject
    MachineDataModel machineDataModel;
    @Inject
    ActivityScreenSwitcher mScreenSwitcher;
    private EditMachineDetailActivity mActivity;
    private long mId;
    private View mView;
    private int mYear, mMonth, mDay;
    private List<String> mImageUris = new ArrayList<>();
    private List<String> mImageUriList = new ArrayList<>();

    public EditMachineDetailPresenter(EditMachineDetailActivity editMachineDetailActivity, long id) {
        mActivity = editMachineDetailActivity;
        mId = id;
    }

    @Override
    public void onLoad(){
       MachineData machineData =  machineDataModel.loadFromID(mId);
        getView().mMachineName.setText(machineData.getMachine_name());
        getView().mMachineType.setText(machineData.getMachine_type());
        getView().mBarcodeNumber.setText(String.valueOf(machineData.getMachine_barcode_number()));
        getView().mLastMaintenanceDate.setText(machineData.getLast_maintenance_date());
        getView().mLastMaintenanceDate.setOnClickListener(view -> setTimePicker(getView().mLastMaintenanceDate,view));
        getView().mMachineImage.setOnClickListener(view -> selectImage());
        if (machineData.getMachine_image_gallery() != null){
            String[] imageUrisTmp = machineData.getMachine_image_gallery().split(",");
            for(int i = 0; i<imageUrisTmp.length;i++){
                mImageUris.add(imageUrisTmp[i]);
            }
            getView().loadImageList(mImageUris);
        }
        getView().mSubmit.setOnClickListener(submit -> updateMachine(machineData));
    }

    private void updateMachine(MachineData machineData){
        StringBuilder mediaUris = new StringBuilder();
        String mMachineName = getView().mMachineName.getText().toString();
        String mMachineType = getView().mMachineType.getText().toString();
        String mMachineBarcodeNumber = getView().mBarcodeNumber.getText().toString();
        String mLastMaintenanceDate = getView().mLastMaintenanceDate.getText().toString();
        machineData.setMachine_name(mMachineName);
        machineData.setMachine_type(mMachineType);
        machineData.setMachine_barcode_number(Integer.parseInt(mMachineBarcodeNumber));
        String startChar = "";
        for(int i = 0;i<getView().getmMachineImageUriList().size(); i++){
            mediaUris.append(startChar);
            String mediaUriItem= getView().getmMachineImageUriList().get(i).toString();
            mediaUris.append(mediaUriItem);
            startChar = ",";
        }
        machineData.setMachine_image_gallery(mediaUris.toString());
        machineData.setLast_maintenance_date(mLastMaintenanceDate);
        machineDataModel.save(machineData);

        UIhelper.SNACKBAR_SHORT(getView(), "Successfully update data");

        mScreenSwitcher.open(new MachineDataActivity.Screen());
    }
    private void selectImage(){
        mActivity.startActivityForResult(new GalleryHelper()
                .setMultiselection(true)
                .setShowVideos(false)
                .setMaxSelectedItems(10)
                .getCallingIntent(mActivity), AppConst.ACTIVITY_REQUEST_CODE.SELECT_PICTURE);
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == AppConst.ACTIVITY_REQUEST_CODE.SELECT_PICTURE) {
            if (resultCode == Activity.RESULT_OK) {
                List<GalleryMedia> galleryMedias =
                        data.getParcelableArrayListExtra(GalleryActivity.RESULT_GALLERY_MEDIA_LIST);
                for (int i = 0;i < galleryMedias.size(); i ++){
                    mImageUriList.add(galleryMedias.get(i).mediaUri());
                }
                getView().loadImageList(mImageUriList);
            }
        }

    }

    private void setTimePicker(EditText editText, View mViewSecond){
        //Clean SoftKey
        mView = mViewSecond;
        mView = mActivity.getCurrentFocus();
        if (mView != null) {
            InputMethodManager imm = (InputMethodManager)mActivity.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(mView.getWindowToken(), 0);
        }
        //Configuration Calendar
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);
        long now = c.getTimeInMillis() / 1000;
        c.getTimeInMillis();
        System.out.println(now);

        DatePickerDialog datePickerDialog = new DatePickerDialog(mActivity,
                (view, year, monthOfYear, dayOfMonth) -> {
                    editText.setText(year + "-" + (monthOfYear + 1) + "-" + dayOfMonth);
                    SimpleDateFormat mDateFormatter = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH);
                    Calendar mCalendar = Calendar.getInstance();
                    String currDate = dayOfMonth + "/" + (monthOfYear + 1) + "/" + year;
                    try {
                        mCalendar.setTime(mDateFormatter.parse(currDate));
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.show();
    }
}
