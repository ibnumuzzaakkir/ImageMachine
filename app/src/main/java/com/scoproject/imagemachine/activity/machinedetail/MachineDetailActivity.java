package com.scoproject.imagemachine.activity.machinedetail;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;

import com.scoproject.imagemachine.activity.machinedata.DaggerMachineDataComponent;
import com.scoproject.imagemachine.activity.machinedata.MachineDataActivity;
import com.scoproject.imagemachine.activity.machinedata.MachineDataPresenter;
import com.scoproject.imagemachine.activity.machinedata.MachineDataView_;
import com.scoproject.imagemachine.di.AppComponent;
import com.scoproject.imagemachine.ui.BaseActivity;
import com.scoproject.imagemachine.ui.ViewPresenter;
import com.scoproject.imagemachine.ui.navigation.ActivityScreen;

/**
 * Created by Ibnu Muzzakkir on 2/1/2017.
 * Contact ibnumuzzaakkir@gmail.com
 * scoproject
 */

public class MachineDetailActivity extends BaseActivity {
    private MachineDetailView mView;
    private MachineDetailComponent mComponent;
    private MachineDetailPresenter mPresenter;

    @Override
    protected void onCreateUI(Bundle bundle) {
        mView = MachineDetailView_.build(this);
        setContentView(mView);
        long mId = getIntent().getLongExtra(Screen.MACHINE_DETAIL_DATA, 0);
        int source = getIntent().getIntExtra(Screen.MACHINE_DETAIL_SOURCE, 0);
        mPresenter = new MachineDetailPresenter(this, mId, source);
        mComponent.inject(mPresenter);
        mPresenter.takeView(mView);
    }

    @Override
    protected void onCreateComponent(AppComponent appComponent) {
        mComponent = DaggerMachineDetailComponent.builder().appComponent(appComponent).build();
        mComponent.inject(this);
    }

    @Override
    protected boolean isValid() {
        return false;
    }

    protected ViewPresenter<? extends View> presenter() {
        return mPresenter;
    }

    public static class Screen extends ActivityScreen {
        private static final String MACHINE_DETAIL_DATA = "machine_detail_data";
        private static final String MACHINE_DETAIL_SOURCE = "machine_detail_source";

        private long mId;
        private int source;

        public Screen(long id, int source) {
            this.mId = id;
            this.source = source;
        }

        @Override
        protected void configureIntent(@NonNull Intent intent) {
            intent.putExtra(MACHINE_DETAIL_DATA, mId);
            intent.putExtra(MACHINE_DETAIL_SOURCE, source);
        }

        @Override
        protected Class<? extends Activity> activityClass() {
            return MachineDetailActivity.class;
        }
    }
}
