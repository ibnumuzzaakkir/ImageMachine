package com.scoproject.imagemachine.activity.machinedetail;

import com.scoproject.imagemachine.di.AppComponent;

import dagger.Component;

/**
 * Created by Ibnu Muzzakkir on 2/1/2017.
 * Contact ibnumuzzaakkir@gmail.com
 * scoproject
 */
@MachineDetailScope
@Component(dependencies = {AppComponent.class})
public interface MachineDetailComponent {
    void inject(MachineDetailActivity mActivity);
    void inject(MachineDetailPresenter mPresenter);
    void inject(EditMachineDetailPresenter mPresenter);
    void inject(EditMachineDetailActivity mActivity);

}
