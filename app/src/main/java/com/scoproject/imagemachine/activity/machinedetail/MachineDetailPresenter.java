package com.scoproject.imagemachine.activity.machinedetail;

import android.support.annotation.NonNull;
import android.widget.Toast;

import com.google.gson.Gson;
import com.scoproject.imagemachine.activity.machinedata.MachineDataActivity;
import com.scoproject.imagemachine.app.AppConst;
import com.scoproject.imagemachine.data.MachineData;
import com.scoproject.imagemachine.model.MachineDataModel;
import com.scoproject.imagemachine.ui.ViewPresenter;
import com.scoproject.imagemachine.ui.navigation.ActivityScreenSwitcher;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

/**
 * Created by Ibnu Muzzakkir on 2/1/2017.
 * Contact ibnumuzzaakkir@gmail.com
 * scoproject
 */

public class MachineDetailPresenter extends ViewPresenter<MachineDetailView> {
    @Inject
    MachineDataModel machineDataModel;

    @Inject
    Gson gson;
    @Inject
    ActivityScreenSwitcher mScreenSwitcher;
    private MachineDetailActivity mActivity;
    private long mId;
    private int mSource;
    private MachineData machineData;
    private List<String> mImageUris = new ArrayList<>();

    public MachineDetailPresenter(MachineDetailActivity machineDetailActivity, long id, int source) {
        mActivity = machineDetailActivity;
        mId = id;
        mSource = source;
    }

    @Override
    public void onLoad(){
        setMachineDataModel(mId);
    }

    private void setMachineDataModel(@NonNull long id){
        if(mSource == AppConst.SOURCE_ACTIVITY.MACHINE_DATA_ACTIVITY){
            machineData = machineDataModel.loadFromID(id);
        }else{
            List<MachineData> machineDatas = machineDataModel.loadFromBarcodeNo((int) id);
            if(machineDatas.size() != 0){
               for(MachineData mmachineData : machineDatas){
                   machineData = mmachineData;
               }
            }else{
                Toast.makeText(mActivity,"Data not found", Toast.LENGTH_SHORT).show();
                mActivity.finish();
            }
        }
        if(machineData != null){
            getView().mTvMachineName.setText(machineData.getMachine_name());
            getView().mTvMachineType.setText(machineData.getMachine_type());
            getView().mTvBarcodeNumber.setText(String.valueOf(machineData.getMachine_barcode_number()));
            getView().mTvMaintenanceDate.setText(machineData.getLast_maintenance_date());
            if (machineData.getMachine_image_gallery() != null){
                String[] imageUrisTmp = machineData.getMachine_image_gallery().split(",");
                for(int i = 0; i<imageUrisTmp.length;i++){
                    mImageUris.add(imageUrisTmp[i]);
                }
                getView().loadImageList(mImageUris);
            }
            getView().mBtnEdit.setOnClickListener(delete -> {
                mScreenSwitcher.open(new EditMachineDetailActivity.Screen(machineData.getId()));
            });

            getView().mBtnDelete.setOnClickListener(edit -> {
                machineDataModel.delete(machineData);
                mScreenSwitcher.open(new MachineDataActivity.Screen());
            });
        }
    }
}
