package com.scoproject.imagemachine.activity.machinedetail;

import android.content.Context;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.scoproject.imagemachine.R;
import com.scoproject.imagemachine.app.AppConst;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ibnu Muzzakkir on 2/1/2017.
 * Contact ibnumuzzaakkir@gmail.com
 * scoproject
 */
@EViewGroup(R.layout.activity_detailmachine)
public class MachineDetailView extends CoordinatorLayout {
    @ViewById(R.id.detailmachine_btn_delete)
    Button mBtnDelete;
    @ViewById(R.id.detailmachine_btn_edit)
    Button mBtnEdit;
    @ViewById(R.id.detailmachine_tv_last_maintenance_date)
    TextView mTvMaintenanceDate;
    @ViewById(R.id.detailmachine_tv_machine_barcode_number)
    TextView mTvBarcodeNumber;
    @ViewById(R.id.detailmachine_tv_machine_name)
    TextView mTvMachineName;
    @ViewById(R.id.detailmachine_tv_machine_type)
    TextView mTvMachineType;
    @ViewById(R.id.detailmachine_rv_gallery_machine)
    RecyclerView mRvGalleryMachine;
    private MachineImageAdapter mAdapter;
    private List<String> mMachineImageUriList = new ArrayList<>();
    private LayoutInflater mLayoutInflater;
    @AfterViews
    void init(){
        mRvGalleryMachine.setLayoutManager(new GridLayoutManager(getContext(), AppConst.LIMIT_IMAGE_GALLERY.limit));
        mRvGalleryMachine.setHasFixedSize(true);
        mAdapter = new MachineImageAdapter();
        mRvGalleryMachine.setAdapter(mAdapter);
    }
    public MachineDetailView(Context context) {
        super(context);
        mLayoutInflater = LayoutInflater.from(getContext());
    }
    void loadImageList(List<String> mImageUriList){
        if(getSizeMachineImage() == 0){
            if(mImageUriList.size() != 0){
                mMachineImageUriList.addAll(mImageUriList);
            }
        }else if(getSizeMachineImage() != 0 && getSizeMachineImage() <=10){
            if(mImageUriList.size() != 0){
                mMachineImageUriList.addAll(mImageUriList);
            }
        }else{
            Log.d(getClass().getName(), "Over Limit Add New Photo Album");
        }
        mAdapter.notifyDataSetChanged();
    }
    List<String> getmMachineImageUriList(){
        return mMachineImageUriList;
    }

    int getSizeMachineImage(){
        return  mMachineImageUriList.size();
    }

    private class MachineImageAdapter extends RecyclerView.Adapter<MachineDetailView.MachineImageViewHolder>{

        @Override
        public MachineDetailView.MachineImageViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = mLayoutInflater.inflate(R.layout.activity_addmachinedata_image_gallery_item, parent, false);
            return new MachineDetailView.MachineImageViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(MachineDetailView.MachineImageViewHolder holder, int position) {
            String galleryItem = mMachineImageUriList.get(position);
            Glide.with(getContext())
                    .load(new File(galleryItem))
                    .centerCrop()
                    .into(holder.mMachineImage);
        }

        @Override
        public int getItemCount() {
            return mMachineImageUriList.size();
        }
    }


    private static class MachineImageViewHolder extends RecyclerView.ViewHolder{
        ImageView mMachineImage;
        public MachineImageViewHolder(View itemView) {
            super(itemView);
            mMachineImage = (ImageView) itemView.findViewById(R.id.addmachinedata_machine_image_gallery);
        }
    }
}
