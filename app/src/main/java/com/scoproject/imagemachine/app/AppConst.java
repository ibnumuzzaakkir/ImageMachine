package com.scoproject.imagemachine.app;

/**
 * Created by Ibnu Muzzakkir on 2/1/2017.
 * Contact ibnumuzzaakkir@gmail.com
 * scoproject
 */

public interface AppConst {
    interface DBNAME{
        String name = "machinedata";
    }
    interface LIMIT_IMAGE_GALLERY{
        int limit = 3;
    }
    interface ACTIVITY_REQUEST_CODE {
        int SELECT_PICTURE = 1;
    }
    interface SOURCE_ACTIVITY{
        int MACHINE_DATA_ACTIVITY = 0;
        int MACHINE_BARCODE_ACTIVITY = 1;
    }
}
