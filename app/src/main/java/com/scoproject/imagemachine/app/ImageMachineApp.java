package com.scoproject.imagemachine.app;

import android.support.multidex.MultiDex;
import android.support.multidex.MultiDexApplication;

import com.scoproject.imagemachine.di.AppComponent;

/**
 * Created by Ibnu Muzzakkir on 1/31/2017.
 * Contact ibnumuzzaakkir@gmail.com
 * scoproject
 */

public class ImageMachineApp extends MultiDexApplication {
    private static ImageMachineApp mInstance;

    private AppComponent mAppComponent;

    public static ImageMachineApp getApp(){ return mInstance; }
    @Override
    public void onCreate(){
        super.onCreate();

        MultiDex.install(this);

        mInstance = this;
        mAppComponent = AppComponent.Initializer.init(this);
    }

    public AppComponent component(){ return mAppComponent;}
}
