package com.scoproject.imagemachine.data;

import com.google.gson.annotations.SerializedName;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Index;
import org.greenrobot.greendao.annotation.Unique;
import org.greenrobot.greendao.annotation.Generated;

/**
 * Created by Ibnu Muzzakkir on 2/1/2017.
 * Contact ibnumuzzaakkir@gmail.com
 * scoproject
 */
@Entity
public class MachineData {
    @Id(autoincrement = true)
    @Unique
    private Long id;
    @SerializedName("machine_name")
    private String machine_name;
    @SerializedName("machine_type")
    private String machine_type;
    @Unique
    @SerializedName("machine_barcode_number")
    private int machine_barcode_number;
    @SerializedName("last_maintenance_date")
    private String last_maintenance_date;
    @SerializedName("machine_image_gallery")
    private String machine_image_gallery;

    @Generated(hash = 560430675)
    public MachineData(Long id, String machine_name, String machine_type,
            int machine_barcode_number, String last_maintenance_date,
            String machine_image_gallery) {
        this.id = id;
        this.machine_name = machine_name;
        this.machine_type = machine_type;
        this.machine_barcode_number = machine_barcode_number;
        this.last_maintenance_date = last_maintenance_date;
        this.machine_image_gallery = machine_image_gallery;
    }

    @Generated(hash = 1351741609)
    public MachineData() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMachine_name() {
        return machine_name;
    }

    public void setMachine_name(String machine_name) {
        this.machine_name = machine_name;
    }

    public String getMachine_type() {
        return machine_type;
    }

    public void setMachine_type(String machine_type) {
        this.machine_type = machine_type;
    }

    public int getMachine_barcode_number() {
        return machine_barcode_number;
    }

    public void setMachine_barcode_number(int machine_barcode_number) {
        this.machine_barcode_number = machine_barcode_number;
    }

    public String getLast_maintenance_date() {
        return last_maintenance_date;
    }

    public void setLast_maintenance_date(String last_maintenance_date) {
        this.last_maintenance_date = last_maintenance_date;
    }

    public String getMachine_image_gallery() {
        return machine_image_gallery;
    }

    public void setMachine_image_gallery(String machine_image_gallery) {
        this.machine_image_gallery = machine_image_gallery;
    }
}
