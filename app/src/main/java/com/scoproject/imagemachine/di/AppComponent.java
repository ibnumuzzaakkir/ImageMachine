package com.scoproject.imagemachine.di;

import android.app.Application;
import android.support.annotation.VisibleForTesting;

import com.google.gson.Gson;
import com.scoproject.imagemachine.app.ImageMachineApp;
import com.scoproject.imagemachine.di.module.AppModule;
import com.scoproject.imagemachine.di.module.AppUIModule;
import com.scoproject.imagemachine.di.scope.ApplicationScope;
import com.scoproject.imagemachine.model.MachineDataModel;

import dagger.Component;

/**
 * Created by Ibnu Muzzakkir on 1/31/2017.
 * Contact ibnumuzzaakkir@gmail.com
 * scoproject
 */
@ApplicationScope
@Component(
        modules = {AppModule.class, AppUIModule.class}
)
public interface AppComponent extends IAppComponent {

    final static class Initializer {
        @SuppressWarnings("deprecation")
        @VisibleForTesting
        public static AppComponent init(ImageMachineApp app) {
            return DaggerAppComponent.builder()
                    .appModule(new AppModule(app))
                    .build();
        }
    }
    Application getApplication();
    Gson getGson();

    /*Inject Model*/
    MachineDataModel getMachineDataModel();
    void inject(MachineDataModel machineDataModel);
}
