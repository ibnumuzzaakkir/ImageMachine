package com.scoproject.imagemachine.di;

import com.scoproject.imagemachine.app.ImageMachineApp;
import com.scoproject.imagemachine.ui.navigation.ActivityScreenSwitcher;

/**
 * Created by Ibnu Muzzakkir on 11/9/2016.
 * Contact ibnumuzzaakkir@gmail.com
 * scoproject
 */
public interface IAppComponent {

    void inject(ImageMachineApp app);

    ActivityScreenSwitcher activityScreenSwitcher();

}
