package com.scoproject.imagemachine.di.module;

import android.app.Application;
import android.database.sqlite.SQLiteDatabase;

import com.google.gson.Gson;
import com.scoproject.imagemachine.app.ImageMachineApp;
import com.scoproject.imagemachine.data.DaoMaster;
import com.scoproject.imagemachine.data.DaoSession;
import com.scoproject.imagemachine.app.AppConst;
import com.scoproject.imagemachine.di.scope.ApplicationScope;
import com.scoproject.imagemachine.model.MachineDataModel;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Ibnu Muzzakkir on 1/31/2017.
 * Contact ibnumuzzaakkir@gmail.com
 * scoproject
 */
@Module
public class AppModule {
    private final ImageMachineApp mApp;

    public AppModule(ImageMachineApp app){ this.mApp = app; }

    @Provides
    @ApplicationScope
    Application provideApplicationContext(){ return mApp; }


    @Provides
    @ApplicationScope
    DaoSession provideDaoSession() {
        String DbName = AppConst.DBNAME.name;
        DaoMaster.DevOpenHelper devOpenHelper = new DaoMaster.DevOpenHelper(ImageMachineApp.getApp(), DbName);
        SQLiteDatabase db = devOpenHelper.getWritableDatabase();
        DaoMaster daoMaster = new DaoMaster(db);
        return daoMaster.newSession();
    }

    @Provides
    @ApplicationScope
    MachineDataModel provideMachineDataModel(DaoSession daoSession) {
        return new MachineDataModel(mApp, daoSession);
    }

    @Provides
    @ApplicationScope
    Gson provideGson(){ return new Gson();}
}
