package com.scoproject.imagemachine.di.module;


import com.scoproject.imagemachine.di.scope.ApplicationScope;
import com.scoproject.imagemachine.ui.navigation.ActivityScreenSwitcher;

import dagger.Module;
import dagger.Provides;


/**
 * Created by Ibnu Muzzakkir on 11/9/2016.
 * Contact ibnumuzzaakkir@gmail.com
 * scoproject
 */
@Module
public class AppUIModule {

    @Provides
    @ApplicationScope
    ActivityScreenSwitcher provideActivityScreenSwitcher() {
        return new ActivityScreenSwitcher();
    }

}
