package com.scoproject.imagemachine.model;

import com.scoproject.imagemachine.app.ImageMachineApp;
import com.scoproject.imagemachine.data.DaoSession;
import com.scoproject.imagemachine.di.AppComponent;

/**
 * Created by Ibnu Muzzakkir on 2/1/2017.
 * Contact ibnumuzzaakkir@gmail.com
 * scoproject
 */

public class BaseModel {
    protected final DaoSession mDaoSession;
    protected final AppComponent mComponent;

    public BaseModel(ImageMachineApp app, DaoSession daoSession) {
        mComponent = app.component();
        mDaoSession = daoSession;
    }
}
