package com.scoproject.imagemachine.model;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.scoproject.imagemachine.app.ImageMachineApp;
import com.scoproject.imagemachine.data.DaoSession;
import com.scoproject.imagemachine.data.MachineData;
import com.scoproject.imagemachine.data.MachineDataDao;

import java.util.List;

/**
 * Created by Ibnu Muzzakkir on 2/1/2017.
 * Contact ibnumuzzaakkir@gmail.com
 * scoproject
 */

public class MachineDataModel extends BaseModel {

    private MachineDataDao mEntityDao;

    public MachineDataModel(ImageMachineApp app, DaoSession daoSession) {
        super(app, daoSession);
        app.component().inject(this);
        mEntityDao = daoSession.getMachineDataDao();

    }
    @NonNull
    public MachineData loadFromID(long id) { return  mEntityDao.load(id);}

    @NonNull
    public List<MachineData> loadFromBarcodeNo(int barcodeNumber) {
        return  mEntityDao.queryBuilder().where(MachineDataDao.Properties.Machine_barcode_number.eq(barcodeNumber)).limit(1).list();}

    @Nullable
    public List<MachineData> loadAll() {
        return mEntityDao.loadAll();
    }

    public void save(MachineData machineData) {
        mEntityDao.insertOrReplace(machineData);
    }

    public void delete(MachineData machineData) {
        mEntityDao.delete(machineData);
    }
}
