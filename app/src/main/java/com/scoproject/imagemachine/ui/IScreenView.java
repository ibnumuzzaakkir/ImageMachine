package com.scoproject.imagemachine.ui;

/**
 * Created by Ibnu Muzzakkir on 1/31/2017.
 * Contact ibnumuzzaakkir@gmail.com
 * scoproject
 */
public interface IScreenView {

    /**
     * Callback to perform view-based customization
     * <p/>
     * You shall NOT put the logic here and all application logic shall go to the
     * presenter instead of view
     */
    public void onShowView();

    public void onHideView();

    public void onDestroy();
}
