package com.scoproject.imagemachine.utils;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Ibnu Muzzakkir on 2/1/2017.
 * Contact ibnumuzzaakkir@gmail.com
 * scoproject
 */

public class TimeHelper {
    public static String getFormatedDate(long ts) {
        Date df = new java.util.Date(ts*1000);
        return new SimpleDateFormat("dd-MM-yyyy").format(df);
    }
    public static String getFormatedDateTime(long ts) {
        Date df = new java.util.Date(ts*1000);
        return new SimpleDateFormat("dd-MM-yyyy HH:mm:ss").format(df);
    }

    public static String getFormatedTime(long ts) {
        Date df = new java.util.Date(ts * 1000);
        return new SimpleDateFormat("HH:mm:ss").format(df);
    }

    public static String getFormatDateFromDevice(Long ts){
        Date df = new java.util.Date(ts);
        return new SimpleDateFormat("dd-MM-yyyy").format(df);
    }
}
