package com.scoproject.imagemachine.utils;

import android.support.design.widget.Snackbar;
import android.view.View;

/**
 * Created by Ibnu Muzzakkir on 2/1/2017.
 * Contact ibnumuzzaakkir@gmail.com
 * scoproject
 */

public class UIhelper {
    public static void SNACKBAR_SHORT(View view, String Text){
        Snackbar snackbar = Snackbar
                .make(view, Text, Snackbar.LENGTH_SHORT);
        snackbar.show();
    }
}
